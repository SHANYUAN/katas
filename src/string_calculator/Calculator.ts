/**
 * Allows mathematical operations on strings of numbers.
 */
export class StringCalculator {
  private readonly delimiter: string = ",";

  /**
   * Returns the sum of a given string of numbers. Throws an exception if
   * negative numbers are used.
   */
  public add(input: string): number {
    if (!input) return 0;
    if (this.hasNegativeNum(input)) {
      throw new Error(
        `negatives not allowed: ${this.getNegativeNumbers(input)}`
      );
    }

    input = this.resolveCustomDelimiters(input);
    input = this.resolveNewLines(input);

    const sum: number = this.getSumForStringOfNumbers(input);
    return sum;
  }

  private resolveNewLines(input: string): string {
    let result: string = input;
    while (result.includes("\n")) {
      result = result.replace("\n", this.delimiter);
    }
    return result;
  }

  private resolveCustomDelimiters(input: string): string {
    if (!this.hasCustomDelimiter(input)) return input;

    const tokens = input.split("\n");
    let customDelimiters: Array<string> = this.getDelimiters(tokens[0]);
    // remove first line
    input = tokens.reduce((acc, cur, i) => {
      if (i === 0) return acc;
      return (acc += cur);
    }, "");

    for (let delimiter of customDelimiters) {
      while (input.includes(delimiter)) {
        input = input.replace(delimiter, this.delimiter);
      }
    }
    return input;
  }

  private hasCustomDelimiter(input: string): boolean {
    return input.startsWith("//") && input.includes("\n");
  }
  private getNegativeNumbers(input: string) {
    let negativeNums: Array<string> = [];
    for (let i = 0; i < input.length; i++) {
      if (input[i] === "-") {
        negativeNums.push(input[i] + input[i + 1]);
      }
    }
    return negativeNums;
  }

  private getSumForStringOfNumbers(input: string): number {
    const numbers: Array<number> = input
      .split(this.delimiter)
      .reduce((tot, cur) => tot.concat(parseInt(cur)), []);

    const sum: number = numbers
      .filter(num => num <= 1000)
      .reduce((tot, cur) => tot + cur);

    return sum;
  }

  private getDelimiters(line: string): Array<string> {
    const delimiters: Array<string> = [];
    if (line.length === 3) {
      // char delimiter
      delimiters.push(line[2]);
    } else {
      const tokens = line.replace("//", "").split("[");

      tokens.forEach((token, index) => {
        if (index > 0) {
          const delimiter = token.replace("]", "");
          delimiters.push(delimiter);
        }
      });
    }
    return delimiters;
  }

  private hasNegativeNum(input: string): boolean {
    return !!input.includes("-");
  }
}
