/**
 * Counts number of lines for Java and JavaScript files. Commented out lines
 * are excluded.
 */
export class CodeLinesCounter {
  public getLineCount(input: string): number {
    const lines: Array<string> = input.split("\n");
    return lines.filter(line => {
      return line.length > 0 && !this.isLineComment(line);
    }).length;
  }

  private isLineComment(line: string): boolean {
    // clear tabs and spaces
    let cleanLine: string = line.replace(/(\t| )/g, "");

    return (
      cleanLine.startsWith("//") ||
      cleanLine.startsWith("/**") ||
      cleanLine.startsWith("*")
    );
  }
}
