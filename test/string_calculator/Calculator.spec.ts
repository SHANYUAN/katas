import { expect } from "chai";
import { StringCalculator } from "../../src/string_calculator/Calculator";

describe("StringCalculator", () => {
  let calculator: StringCalculator;

  beforeEach(() => {
    calculator = new StringCalculator();
  });

  it("should return zero given empty string", () => {
    const result = calculator.add("");
    expect(result).to.eql(0);
  });

  it("should return sum given single number", () => {
    const result = calculator.add("1");
    expect(result).to.eql(1);
  });

  it("should return sum given two numbers separated by comma", () => {
    const result = calculator.add("1,2");
    expect(result).to.eql(3);
  });

  it("should return sum given three numbers separated by commas or new lines", () => {
    const result = calculator.add("1\n2,3");
    expect(result).to.eql(6);
  });

  it("should return sum given explicit delimiter in first line", () => {
    const result = calculator.add("//;\n1;2");
    expect(result).to.eql(3);
  });

  it("should throw exception given negative number", () => {
    expect(() => {
      calculator.add("-1");
    }).to.throw("negatives not allowed: -1");
  });

  it("should throw exception given multiple negative numbers", () => {
    expect(() => {
      calculator.add("-1, -2");
    }).to.throw("negatives not allowed: -1,-2");
  });

  it("should return sum ignoring numbers > 1000", () => {
    const result = calculator.add("2,1001");
    expect(result).to.eql(2);
  });

  it("should return sum given delimiter of any length", () => {
    const result = calculator.add("//[***]\n1***2***3");
    expect(result).to.eql(6);
  });

  it("should return sum given multiple delimiters", () => {
    const result = calculator.add("//[*][%]\n1*2%3”");
    expect(result).to.eql(6);
  });

  it("should return sum given multiple longer delimiters", () => {
    const result = calculator.add("//[***][%%%%]\n1***2%%%%3”");
    expect(result).to.eql(6);
  });

  it("should return sum given multiple new lines", () => {
    const result = calculator.add("1\n2\n3”");
    expect(result).to.eql(6);
  });
});
