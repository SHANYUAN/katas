interface IRule {
  price: number;
  specialCount: number;
  specialPrice: number;
}

export class Checkout {
  private rules: Object;
  private total: number = 0;
  private items: Array<string> = [];

  constructor(rules: Object) {
    this.rules = rules;
  }

  public scan(item: string): void {
    const rule: IRule = this.rules[item];
    if (!rule) return;

    this.items.push(item);

    const itemCount: number = this.items.reduce((acc, cur) => {
      if (cur === item) return (acc = acc + 1);
      return acc;
    }, 0);

    const isSpecial: boolean = itemCount % rule.specialCount === 0;
    if (isSpecial) {
      this.total += rule.specialPrice - (rule.specialCount - 1) * rule.price;
    } else {
      this.total += rule.price;
    }
  }

  public getTotal(): number {
    return this.total;
  }
}
