import { expect } from "chai";
import { CodeLinesCounter } from "../../src/code_counter/CodeLinesCounter";

describe("CodeLinesCounter", () => {
  let counter: CodeLinesCounter;

  beforeEach(() => {
    counter = new CodeLinesCounter();
  });

  it("given empty content should return zero", () => {
    const count: number = counter.getLineCount("");
    expect(count).to.eql(0);
  });

  it("should count lines", () => {
    const content = [
      "public interface App {",
      "\tint getSomeNumber(File inFile);",
      "}"
    ].join("\n");
    const count: number = counter.getLineCount(content);
    expect(count).to.eql(3);
  });

  it("should ignore blank lines", () => {
    const content = [
      "public interface App {",
      "\tint getSomeNumber(File inFile);",
      "",
      "}"
    ].join("\n");
    const count: number = counter.getLineCount(content);
    expect(count).to.eql(3);
  });

  it("should ignore line comment", () => {
    const content = [
      "// some comment",
      "public interface App {",
      "\tint getSomeNumber(File inFile);",
      "};"
    ].join("\n");

    const count: number = counter.getLineCount(content);
    expect(count).to.eql(3);
  });

  it("should ignore indented commented line comment", () => {
    const content = [
      "public interface App {",
      "\t// some comment",
      "\tint getSomeNumber(File inFile);",
      "}"
    ].join("\n");

    const count: number = counter.getLineCount(content);
    expect(count).to.eql(3);
  });

  it("should ignore multiline comment", () => {
    const content = [
      "/**",
      " * This is a multiline comment.",
      " */",
      "public interface App {",
      "\t// some comment",
      "\tint getSomeNumber(File inFile);",
      "}"
    ].join("\n");

    const count: number = counter.getLineCount(content);
    expect(count).to.eql(3);
  });

  it("should ignore indented multiline comment", () => {
    const content = [
      "public interface App {",
      "\t/**",
      "\t * This is a multiline comment.",
      "\t */",
      "\tint getSomeNumber(File inFile);",
      "}"
    ].join("\n");

    const count: number = counter.getLineCount(content);
    expect(count).to.eql(3);
  });

  it("should handle incorrect indentation", () => {
    const content = [
      " /*****",
      " * This is a test program with 5 lines of code",
      " *  \\/* no nesting allowed!",
      " //*****//***/// Slightly pathological comment ending...",
      "",
      "public class Hello {",
      "\tpublic static final void main(String [] args) { // gotta love Java",
      "\t\t// Say hello",
      '\t  System./*wait*/out./*for*/println/*it*/("Hello/*");',
      "\t}",
      "",
      "}"
    ].join("\n");
    const count: number = counter.getLineCount(content);
    expect(count).to.eql(5);
  });
});
