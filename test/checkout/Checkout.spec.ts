import { expect } from "chai";
import { Checkout } from "../../src/checkout/Checkout";

const rules = {
  A: {
    price: 50,
    specialCount: 3,
    specialPrice: 130
  },
  B: {
    price: 30,
    specialCount: 2,
    specialPrice: 45
  },
  C: {
    price: 20
  },
  D: {
    price: 15
  }
};

describe("Checkout", () => {
  let checkout: Checkout;

  function scan(items: string): void {
    items.split("").forEach(item => checkout.scan(item));
  }

  beforeEach(() => {
    checkout = new Checkout(rules);
  });

  it("should return zero given empty item", () => {
    checkout.scan("");
    expect(checkout.getTotal()).to.eql(0);
  });

  it("should return item price given single item", () => {
    checkout.scan("A");
    expect(checkout.getTotal()).to.eql(50);
  });

  it("should return item price given multiple different items", () => {
    scan("AB");
    expect(checkout.getTotal()).to.eql(80);
  });

  it("should return item price given multiple different items", () => {
    scan("CDBA");
    expect(checkout.getTotal()).to.eql(115);
  });

  it("should return item price given same item multiple times", () => {
    scan("AA");
    expect(checkout.getTotal()).to.eql(100);
  });

  it("should return special price given special", () => {
    scan("AAA");
    expect(checkout.getTotal()).to.eql(130);
  });

  it("should return special price given special", () => {
    scan("AAAA");
    expect(checkout.getTotal()).to.eql(180);
  });

  it("should return special price given special", () => {
    scan("AAAAA");
    expect(checkout.getTotal()).to.eql(230);
  });

  it("should return special price given special", () => {
    scan("AAAAAA");
    expect(checkout.getTotal()).to.eql(260);
  });

  it("should return special price given special", () => {
    scan("AAAB");
    expect(checkout.getTotal()).to.eql(160);
  });

  it("should return special price given multiple specials", () => {
    scan("AAABB");
    expect(checkout.getTotal()).to.eql(175);
  });

  it("should return special price given multiple specials", () => {
    scan("AAABBD");
    expect(checkout.getTotal()).to.eql(190);
  });

  it("should return special price given multiple specials", () => {
    scan("DABABA");
    expect(checkout.getTotal()).to.eql(190);
  });
});
